Demonstrates sqlite's `except` functionality by diffing two large randomly generated tables with 50% overlap.

To run:

``` sh
sqlite3 to-diff.db < setup.sql
sqlite3 to-diff.db < diff-tables.sql
```

