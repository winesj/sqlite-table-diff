with left_diff as (
    select * from left except select * from right
), right_diff as (
    select * from right except select * from left
)
select count(*), 'left' as which from left_diff
union all
select count(*), 'right' as which from right_diff;
