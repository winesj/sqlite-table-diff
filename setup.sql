pragma synchronous = off;
PRAGMA journal_mode = wal;

drop table if exists left;
drop table if exists right;

CREATE TABLE left (
  x text NOT NULL,
  y text NOT NULL,
  z text NOT NULL,
  primary key (x, y, z)
) without rowid;


CREATE TABLE right (
  x text NOT NULL,
  y text NOT NULL,
  z text NOT NULL,
  primary key (x, y, z)
) without rowid;

INSERT INTO left
      SELECT hex(randomblob(50)),hex(randomblob(50)), hex(randomblob(50)) from generate_series(1, 1000000);

INSERT INTO right
      SELECT hex(randomblob(50)),hex(randomblob(50)), hex(randomblob(50)) from generate_series(1, 500000)
      UNION
      select * from left where random() < 0;
